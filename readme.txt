
imagefield_import.module will allow users with proper access authorities to be able to import a Bunch of images they upload to an import directory into nodes with Imagefields.  

The user can configure the node type and target imagefield that the images will be imported 
using the settings form at admin/settings/imagefield_import
